import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class PageTitle extends React.Component {
    render() {
        var title = this.props.title;
        return (
            <View style={[styles.container]}>
                <View style={{
                    flex:.1,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#ccc"
                    }}>
                    <Text>{title}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: .8
    }
});
