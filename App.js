import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, FlatList } from 'react-native';
import MovieCard from './src/shared/movie-card/movie-card';
import PageTitle from './src/shared/page-title/page-title';

export default class App extends React.Component {
  getMovieTemplate(movie) {
    return (
      <MovieCard
          movie={movie}
          styles={{
            backgroundColor: "#fff"
          }}
        />
    )
  }

  render() {
    var movies = [{
      id: 1,
      title: "Homem-Formiga e a Vespa",
      year: "2019",
      overview: "A vida de Scott Lang (Paul Rudd) ficou bem agitada desde que se transformou no Homem-Fomiga. Ele precisa encontrar uma forma de equilibrar a vida de super-herói com a de pai. É quando ele recebe uma importante e arriscada missão, tendo como aliada Hope/Vespa (Evangeline Lilly).",
      imageUrl: "https://image.tmdb.org/t/p/w250_and_h141_face/6P3c80EOm7BodndGBUAJHHsHKrp.jpg"
    },
    {
      id: 2,
      title: "Homem-Formiga",
      year: "2019",
      overview: "A vida de Scott Lang (Paul Rudd) ficou bem agitada desde que se transformou no Homem-Fomiga. Ele precisa encontrar uma forma de equilibrar a vida de super-herói com a de pai. É quando ele recebe uma importante e arriscada missão, tendo como aliada Hope/Vespa (Evangeline Lilly).",
      imageUrl: "https://image.tmdb.org/t/p/w250_and_h141_face/6P3c80EOm7BodndGBUAJHHsHKrp.jpg"
    }]
    return (
      <View style={styles.container}>
        <PageTitle title={"Listagem de Filmes"} />
        <FlatList
            data={movies}
            renderItem={({item}) => this.getMovieTemplate(item)}
            keyExtractor={(item, index) => item.id.toString()}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 16
  },
  titleConfig: {
    fontSize: 20,
    color: "red",
    paddingBottom: 8,
    backgroundColor: "yellow"
  }
});
